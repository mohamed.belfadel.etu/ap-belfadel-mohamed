#belfadel mohamed
#TP4(Gestion d une promotion)
#MI15


def pour_tous(seq_bool:list[bool])->bool:
    """Renvoie True ssi `seq_bool` ne contient pas False

    Précondition : 
    Exemple(s) :
    
    $$$ pour_tous([])
    True
    $$$ pour_tous((True, True, True))
    True
    $$$ pour_tous((True, False, True))
    False
    """
    return not False in seq_bool

def il_existe(seq_bool:list[bool])->bool:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ il_existe([])
    False
    $$$ il_existe([False, True, False])
    True
    $$$ il_existe([False, False])
    False


    """
    return True in seq_bool

from etudiant import Etudiant
from date import Date


def charge_fichier_etudiants(fname: str) -> list[Etudiant]:
    """Renvoie la liste des étudiants présents dans le fichier dont
        le nom est donné en paramètre.

    Précondition : le fichier est du bon format.
    Exemple(s) :
    $$$ 

    """
    res = []
    with open(fname, 'r') as fin:
        fin.readline()
        ligne = fin.readline()
        while ligne != '':
            nip, nom, prenom, naissance, formation, groupe = ligne.strip().split(';')
            y, m, d = naissance.split('-')
            date_naiss = Date(int(d.lstrip('0')), int(m.lstrip('0')), int(y))
            res.append(Etudiant(int(nip), nom, prenom, date_naiss, formation, groupe))
            ligne = fin.readline()
    return res

L_ETUDIANTS=charge_fichier_etudiants("etudiants.csv")
COURTE_LISTE=L_ETUDIANTS[0:11]


def est_liste_d_etudiants(x)->bool:
    """ Renvoie True si ``x`` est une liste de d'étudiant, False dans le cas contraire.

    Précondition : aucune
    Exemple(s) :
    $$$ est_liste_d_etudiants(COURTE_LISTE)
    True
    $$$ est_liste_d_etudiants("Timoleon")
    False
    $$$ est_liste_d_etudiants([('12345678', 'Calbuth', 'Raymond', 'Danse', '12') ])
    False
 

    """
    
    return all(isinstance(i,Etudiant) for i in x)


#4
#>>> len(L_ETUDIANTS)
#603

NBRE_ETUDIANTS = len(L_ETUDIANTS)

#5
NIP = 42323978

NIP % NBRE_ETUDIANTS

#6

def ensemble_des_formations(liste: list[Etudiant]) -> set[str]:
    """
    Renvoie un ensemble de chaînes de caractères donnant les formations
    présentes dans les fiches d'étudiants

    Précondition: liste ne contient que des fiches d'étudiants

    Exemples:

    $$$ ensemble_des_formations(COURTE_LISTE)
    {'MI', 'PEIP', 'MIASHS', 'LICAM'}
    $$$ ensemble_des_formations(COURTE_LISTE[0:2])
    {'MI', 'MIASHS'}
    """
    return {L_ETUDIANTS[i].formation for i in range(len(liste))}


def nb_occu(liste):
    """donne le nb occurence du prenom des etudiants de la promo

    Précondition : 
    Exemple(s) :
    $$$ nb_occu(L_ETUDIANTS)['Camille']
    2
    $$$ nb_occu(L_ETUDIANTS)['Louis']
    2
    $$$ nb_occu(L_ETUDIANTS)['Alexandre']

    """
    
    res = {}
    for etud in liste:
        prenom = etud.prenom
        if prenom in res:
            res[prenom] = res[prenom] + 1
        else:
            res[prenom] = 1
    return res

# il y'a 3 Alexandre et 2 Camille
#la liste necessite un parcours
#9
#il y'a 190 prenoms differents

#10



    

    




    