#belfadel mohamed
#MI15
#fractales

import turtle
from turtle import*

def zigzag(n):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    if n>0:
        turtle.right(45)
        turtle.forward(50)
        turtle.left(90)
        turtle.forward(50)
        turtle.right(45)
        return zigzag(n-1)
    
#Courbe de Von Koch
    
def koch(l:int, n:int):
    """trace la courbe de von koch de longueur l à l'ordre n

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    if n==0:
        turtle.forward(l)
    else:
        koch(l/3,n-1)
        turtle.left(60)
        koch(l/3,n-1)
        turtle.right(120)
        koch(l/3,n-1)
        turtle.left(60)
        koch(l/3,n-1)
        
        


        