from PIL import Image

import traite_img

def main():
    # valeurs statics pour nom_image , order et action
    
    nom_image = "cal.png"
    ordre = 3 #exemple d'ordre 3
    action = "affiche"

    # Charger l'image
    
    input_image = Image.open(nom_image)
    
    # Traiter l'image
    output_image = traite_img.process_image(input_image, ordre)

    # Afficher l'image en fonction de l'action
    if action == "affiche":
        output_image.show()
    else:
        print("Action non valide. L'action valide est 'affiche'.")

if __name__ == "__main__":
    main()

