- - -
Title: Projet  
Author: BELFADEL Mohamed
- - -
#journal

24/03/2024 :

main : 

 j'ai structuré mon projet en créant un fichier main.py dont le script charge une image, la traite, puis effectue l'action d'affichage en fonction des paramètres fournis, ensuite je cree un module nommé traite_img.py qui s'occupe du traitement de l'image
 
traite_img : 
 
 ce module contient les fonctions naicessaires au traitement des images, il contient la fonction principale process_image() qui prend une image et un ordre en entrée.L'ordre détermine le niveau de récursivité de l'algorithme. À chaque niveau de récursion, l'image est divisée en quatre blocs, et l'algorithme est appliqué récursivement à chaque bloc. Ensuite, il vérifie si les blocs sont suffisamment proches en couleur. S'ils le sont, il crée un bloc uniforme avec la couleur moyenne. Sinon, il fusionne les quatre blocs.
 
 26/03/2024 : 

j'ai reflechi à la structure du module traite_img et j'ai codé la fonction process_img qui s'occupe du traitement totale de l'image en faisant appel à d'autres fonction dont chaqu'une s'occupe d'une tache specifique et qu'ils sont les suivants : 

 - diviser_image() : cette fonction divise l'image en 4 blocs
 - average_rgb() : Cette fonction calcule la couleur moyenne de l'image en termes de composantes (RGB)
 - sont_blocs_proches() : qui calcule la moyenne de chaque bloc et verifie si les couleurs sont assez proches, dans ce cas j'ai pris un seuil de 20 pixels
 - creer_bloc_uniforme() : cette fonction cree un bloc uniforme à partir d'une liste de blocs d'images en utilisant la couleur moyenne des blocs
 - fusionner_blocs() : Cette fonction prend une liste de quatre blocs d'images et les fusionne pour créer une seule image contenant ces quatre blocs
 
 30/02/2024 :
 
 .j'ai codé la fonction diviser_image() en utilisant la methode corp qui permet de découper une région rectangulaire spécifique d'une image.dans ce cas là en 4 parties (haut à gauche , haut à droite , bas à gauche , bas à droite)

 .j'ai codé la fonction average_rgb() qui calcule la couleur moyenne d'une image en termes de composantes (Rouge, Vert, Bleu - RGB)
 .j'ai codé la fonction sont_blocs_proches() qui calcule la couleur moyenne de chaque bloc et verifie si les couleurs sont assez proches avec un seuil de 20 pixels
 
 31/04/2024 : 
 .j'ai codé la fonction creer_bloc_uniforme() qui renvoie une nouvelle image représentant la couleur moyenne de tous les blocs
 
 01/04/2024 : 
 .j'ai codé la fonction fusionner_blocs() prend une liste de quatre blocs d'images et les fusionne pour créer une seule image contenant ces quatre blocs

