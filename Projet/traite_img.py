from PIL import Image, ImageDraw

def process_image(image, ordre):
    """cette fonction devise l'image en 4 blocs à chaque niveau de recursion ensuite, il vérifie si les blocs sont suffisamment proches en couleur. S'ils le sont, il crée un bloc uniforme avec la couleur moyenne. Sinon, il fusionne les quatre blocs.
  

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    
    # Cas de base : si l'ordre est zéro, crée et renvoie un bloc de la couleur moyenne de l'image
    if ordre == 0:
        largeur, hauteur = image.size        
        couleur_moyenne_rgb = average_rgb(image)
        bloc_uniforme = Image.new("RGB", (largeur, hauteur), couleur_moyenne_rgb)
        return bloc_uniforme

    # Diviser l'image en quatre blocs
    blocs = diviser_image(image)

    # Appliquer l'algorithme à chaque bloc récursivement
    blocs_traites = []
    for bloc in blocs:
        bloc_traite = process_image(bloc, ordre - 1)
        blocs_traites.append(bloc_traite)

    # Vérifier si les blocs sont proches en couleur
    if sont_blocs_proches(blocs_traites):
        # Si c'est le cas, créer un bloc uniforme avec la couleur moyenne
        return creer_bloc_uniforme(blocs_traites)
    else:
        # Sinon, créer un bloc contenant les quatre blocs traités
        return fusionner_blocs(blocs_traites)
    
    

def diviser_image(image):
    """cette fonction divise l'image en 4 blocs

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    
    width, height = image.size
    half_width = width // 2
    half_height = height // 2
    
    block1 = image.crop((0, 0, half_width, half_height)) # en haut à gauche
    block2 = image.crop((half_width, 0, width, half_height)) # en haut à droite
    block3 = image.crop((0, half_height, half_width, height)) # en bas à gauche
    block4 = image.crop((half_width, half_height, width, height)) # en bas à droite
    
    return [block1, block2, block3, block4]
    


def average_rgb(image):
    """Cette fonction calcule la couleur moyenne de l'image en termes de composantes (RGB)

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    # Chargez l'image et convertissez-la en mode RGB si elle n'est pas déjà dans ce mode
    image = image.convert('RGB')
    
    # Obtenez la largeur et la hauteur de l'image
    width, height = image.size
    
    # Obtenez les données de pixels pour l'ensemble de l'image
    pixels = image.load()
    
    # Initialisez les variables pour stocker la somme des valeurs RGB
    total_r, total_g, total_b = 0, 0, 0
    
    # Parcourez chaque pixel de l'image et additionnez les valeurs RGB
    for y in range(height):
        for x in range(width):
            r, g, b = pixels[x, y]  # Obtenez les valeurs RGB du pixel actuel
            total_r += r
            total_g += g
            total_b += b
    
    # Calculez les valeurs RGB moyennes en divisant la somme totale par le nombre de pixels
    nb_pixels = width * height
    average_r = total_r // nb_pixels
    average_g = total_g // nb_pixels
    average_b = total_b // nb_pixels
    
    return (average_r, average_g, average_b)

    
    
def sont_blocs_proches(blocs):
    """qui calcule la moyenne de chaque bloc et verifie si les couleurs sont assez proches,
       dans ce cas j'ai pris un seuil de 20 pixels

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    # Calculer la couleur moyenne de chaque bloc
    couleurs = [average_rgb(bloc) for bloc in blocs]

    # Vérifier si les couleurs sont assez proches (seuil de 20 pixels)
    
    seuil = 20
    for i in range(len(couleurs) - 1):
        for j in range(i + 1, len(couleurs)):
            couleur1 = couleurs[i]
            couleur2 = couleurs[j]
            if abs(couleur1[0] - couleur2[0]) > seuil or \
               abs(couleur1[1] - couleur2[1]) > seuil or \
               abs(couleur1[2] - couleur2[2]) > seuil:
                return False
    return True

    

def creer_bloc_uniforme(blocs):
    """cette fonction cree un bloc uniforme à partir d'une liste de blocs d'images en utilisant la couleur moyenne des blocs

    Précondition : 
    Exemple(s) :
    $$$ 

    """
     # Calculer les valeurs moyennes de chaque composante RGB pour chaque bloc
    rgb_blocs = [average_rgb(bloc) for bloc in blocs]

    # Calculer la somme des valeurs de chaque composante RGB pour tous les blocs
    #sum_rgb = [sum(values) for values in zip(*rgb_blocs)]

    # Transposer la liste des blocs RGB
    transposed_rgb_blocs = zip(*rgb_blocs)
    print("transposed_rgb_blocs :", transposed_rgb_blocs)

    # Calculer la somme des valeurs de chaque composante RGB pour chaque pixel
    sum_rgb_values = [sum(rgb_values) for rgb_values in transposed_rgb_blocs]

    # Convertir la somme en une liste de valeurs RGB
    sum_rgb = list(sum_rgb_values)

    # Calculer le nombre total de blocs
    nb_blocs = len(blocs)

    # Calculer les valeurs moyennes de chaque composante RGB pour tous les blocs
    average_rgb_values = [total // nb_blocs for total in sum_rgb]

    # Convertir les valeurs moyennes en un tuple
    couleur_moyenne_rgb = tuple(average_rgb_values)

    # Créer une nouvelle image avec la couleur moyenne
    largeur, hauteur = blocs[0].size
    bloc_uniforme = Image.new("RGB", (largeur, hauteur), couleur_moyenne_rgb)
    return bloc_uniforme

def fusionner_blocs(blocs):
    """Cette fonction prend une liste de quatre blocs d'images et les fusionne pour créer une seule image contenant ces quatre blocs

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    # Créer une nouvelle image contenant les quatre blocs
    
    largeur, hauteur = blocs[0].size #extraire la dimension d'un bloc quelconque (dim_bloc1 = dim_bloc2 = dim_bloc3 = dim_bloc4)
    blocs_en_ligne = [blocs[:2], blocs[2:]] #  divise la liste blocs en deux parties, créant ainsi une liste de deux sous-listes où chaque sous-liste contient deux blocs d'images
    
    bloc_fusionne_haut = Image.new("RGB", (largeur * 2, hauteur)) # creer deux images vides avec une largeur deux fois plus grande que celle d'un bloc d'image individuel, mais la même hauteur que les blocs d'origine.
    bloc_fusionne_bas = Image.new("RGB", (largeur * 2, hauteur))
    
    bloc_fusionne_haut.paste(blocs_en_ligne[0][0], (0, 0)) # colle le premier bloc d'image de la première sous-liste dans l'image fusionnée bloc_fusionne_haut
    bloc_fusionne_haut.paste(blocs_en_ligne[0][1], (largeur, 0)) # colle le deuxième bloc d'image de la première sous-liste dans l'image fusionnée bloc_fusionne_haut
    bloc_fusionne_bas.paste(blocs_en_ligne[1][0], (0, 0)) # colle le premier bloc d'image de la deuxième sous-listedans l'image fusionnée bloc_fusionne_bas
    bloc_fusionne_bas.paste(blocs_en_ligne[1][1], (largeur, 0)) # colle le deuxième bloc d'image de la deuxième sous-liste dans l'image fusionnée bloc_fusionne_bas
    
    bloc_fusionne = Image.new("RGB", (largeur * 2, hauteur * 2)) # crée une nouvelle image vide bloc_fusionne avec une largeur et une hauteur deux fois plus grandes que celles des images fusionnées individuelles.
    bloc_fusionne.paste(bloc_fusionne_haut, (0, 0)) # colle l'image fusionnée bloc_fusionne_haut dans l'image bloc_fusionne
    bloc_fusionne.paste(bloc_fusionne_bas, (0, hauteur)) # colle l'image fusionnée bloc_fusionne_bas dans l'image bloc_fusionne juste en dessous du bloc_fusionne_haut
    
    return bloc_fusionne
   


    
