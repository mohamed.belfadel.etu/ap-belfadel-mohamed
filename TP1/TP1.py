#mohamed belfadel
#MI15
#TP1

#methode split:
#1
s="la méthode split est parfois bien utile"
#s.split(' ')=['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
#s.split('e')=['la méthod', ' split ', 'st parfois bi', 'n util', '']
#s.split('é')=['la m', 'thode split est parfois bien utile']
#s.split()=['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
#s.split('')=ValueError: empty separator
#s.split('split')=['la méthode ', ' est parfois bien utile']

#2
#la methode split utilise des separateur passé en parametre pour separer les caracteres de la chaine à l'endroit du separateur dans la chaine

#3 non elle ne la modifie pas

#methode join:
#1
l = s.split()
#"".join(l)='laméthodesplitestparfoisbienutile'
#" ".join(l)='la méthode split est parfois bien utile'
#";".join(l)='la;méthode;split;est;parfois;bien;utile'
#" tralala ".join(l)='la tralala méthode tralala split tralala est tralala parfois tralala bien tralala utile'
#print ("\n".join(l))=
#la
#méthode
#split
#est
#parfois
#bien
#utile
#"".join(s)='la méthode split est parfois bien utile'
#"!".join(s)='l!a! !m!é!t!h!o!d!e! !s!p!l!i!t! !e!s!t! !p!a!r!f!o!i!s! !b!i!e!n! !u!t!i!l!e'
#"".join([])=''
#"".join([1,2])=erreur str attendue

#2
#la méthode join est utilisée pour joindre les éléments d'une collection en une seule chaîne de caractères.

#3
#non

#methode sort:
#1
l=list('timoleon')
#l.sort()=['e', 'i', 'l', 'm', 'n', 'o', 'o', 't']
s="Je n'ai jamais joué de flûte."
l=list(s)
#l.sort()=[' ', ' ', ' ', ' ', ' ', "'", '.', 'J', 'a', 'a', 'a', 'd', 'e', 'e', 'e', 'f', 'i', 'i', 'j', 'j', 'l', 'm', 'n', 'o', 's', 't', 'u', 'é', 'û']
#apres application de la methode sort les caracteres sont rangés en ordre alphabatique

#2
l = ['a', 1]
#erreur is faut avoir soit un caractere soit un entier en parametre mais pas les deux

def sort(s:str)->str:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ sort('timoleon')
    'eilmnoot'

    """
    l=list(s)
    l.sort()
    chaine=""
    for i in range(len(l)):
        chaine+=l[i]
    return chaine

#anagrammes

def sont_anagrammes1(ch1:str , ch2:str)->bool:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ sont_anagrammes1('orange', 'organe')
    True
    $$$ 

    """
    l1=list(ch1)
    l2=list(ch2)
    l1.sort()
    l2.sort()
    return l1==l2
            

    





